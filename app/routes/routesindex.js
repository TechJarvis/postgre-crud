module.exports = app => {
  const employees = require("../controllers/employee.js");

  var router = require("express").Router();

  router.post("/create", employees.create);
  router.get("/find", employees.findAll);
  router.get("/active", employees.findAllActive);
  router.get("/findone", employees.findOne);
  router.post("/update", employees.update);
  router.post("/delete", employees.delete);
  router.post("/deleteall", employees.deleteAll);

  app.use("/api/employees", router);
};
