const db = require("../models");
const Employee = db.employees;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.name) {
    res.status(400).send({
      message: "Name is required!"
    });
    return;
  }
  const employee = {
    name: req.body.name,
    dept: req.body.dept,
    status: req.body.status ? req.body.status : false
  };
  Employee.create(employee)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the record."
      });
    });
};

exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;

  Employee.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
  res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving records."
      });
    });
};

// Find a single record with an id
exports.findOne = (req, res) => {
  const id = req.headers["id"];

  Employee.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving record with id=" + id
      });
    });
};

// Update a record by the id in the request
exports.update = (req, res) => {
  const id = req.headers["id"];

  Employee.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Employee was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update record with id=${id}. Maybe record was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating record with id=" + id
      });
    });
};

// Delete a record with the specified id in the request
exports.delete = (req, res) => {
  const id = req.headers["id"];
  // const id = req.params.id;

  Employee.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Employee was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete record with id=${id}. Maybe record was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete record with id=" + id
      });
    });
};

// Delete all records from the table.
exports.deleteAll = (req, res) => {
  Employee.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} employee records were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all records."
      });
    });
};

// find all active records
exports.findAllActive = (req, res) => {
  Employee.findAll({ where: { status: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving records."
      });
    });
};
